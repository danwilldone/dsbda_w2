import org.junit.Test;

import javax.cache.Cache;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestClass {
    @Test
    public void testString() {
        String x = "one,two,three";
        String[] test = x.trim().split(",");
        Arrays.stream(test).forEach(s -> {
            System.out.println(s);
        });
    }
}
