import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CachePeekMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.lang.IgniteCallable;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.cache.Cache;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static String cacheName = "myCache";

    public static void main(String... args) throws IOException {
        String datapath = args[0];
        String outpath = args[1];
        // 3 servers + 1 client
        for (int i = 0; i <= 3; i++) {
            String springCfgPath = "classpath:example-default.xml";
            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(springCfgPath);

            IgniteConfiguration igniteCfg = ctx.getBean(IgniteConfiguration.class);
            if (i != 3) {
                igniteCfg.setIgniteInstanceName("server_node_" + (i+1));
            }else{
                igniteCfg.setClientMode(true);
                igniteCfg.setIgniteInstanceName("client_node");
            }


            DataStorageConfiguration storageCfg = new DataStorageConfiguration();
            storageCfg.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
            igniteCfg.setDataStorageConfiguration(storageCfg);
            igniteCfg.setPeerClassLoadingEnabled(true);

            Ignite ignite = Ignition.start(igniteCfg);
            // activate cluster after using native persistence
            ignite.cluster().active(true);

            CacheConfiguration<TimestampId, Long> cfg = new CacheConfiguration<>();
            cfg.setCacheMode(CacheMode.PARTITIONED);
            cfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_ASYNC);
            cfg.setName(cacheName);
            cfg.setBackups(1);
            IgniteCache<TimestampId, Long> cache = ignite.getOrCreateCache(cfg);

            boolean isClient = ignite.configuration().isClientMode();

            // compute if on client
            if (isClient) {
                File fout = new File(datapath);
                Scanner scan = new Scanner(fout);
                while (scan.hasNextLine()) {
                    String line = scan.nextLine();
                    String[] parse = line.trim().split(",");
                    TimestampId key = new TimestampId(parse[0], parse[1]);
                    cache.put(key, Long.valueOf(parse[2]));
                }

                Collection<Map<String, Long>> results = ignite.compute().broadcast((IgniteCallable<Map<String, Long>>) () -> {
                    Ignite localIgnite = Ignition.localIgnite();
                    IgniteCache<TimestampId, Long> igniteCache = localIgnite.getOrCreateCache(cacheName);
                    Iterable<Cache.Entry<TimestampId, Long>> entries = igniteCache.localEntries(CachePeekMode.PRIMARY);
                    List<Cache.Entry<TimestampId, Long>> target = new ArrayList<>();
                    entries.forEach(target::add);
                    Map<String, Long> collected = target.stream()
                            .collect(Collectors.toMap(
                                    entry -> entry.getKey().getId(),
                                    entry -> {
                                        TimestampId tid = entry.getKey();
                                        String id = tid.getId();
                                        return target.stream()
                                                .filter(entryInner -> entryInner.getKey().getId().equals(id))
                                                .mapToLong(Cache.Entry::getValue).sum();
                                    }, Math::max));
                    return collected;
                });

                // form the output
                File file = new File(outpath);
                FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fos);
                results.forEach(map -> {
                    map.forEach((key, value) -> {
                        try {
                            osw.write(key + "," + value + "\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                });
                osw.close();
                cache.clear();

            } else {
                // we should add server node to baseline topology
                Collection<ClusterNode> nodes = ignite.cluster().forServers().nodes();
                ignite.cluster().setBaselineTopology(nodes);
            }
        }
        System.exit(0);

    }
}
