import org.apache.ignite.cache.affinity.AffinityKeyMapped;

public class TimestampId {
    @AffinityKeyMapped
    private final String id;
    private final String timestamp;

    public TimestampId(String id, String timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
