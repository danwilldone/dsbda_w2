
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
import csv
from random import randint
import subprocess
import os


# ### Dataset generation

# In[ ]:


print('Please, input path to your artificial dataset:')
raw_data_path = input()
print('\n')
print('Input path to result file:')
ignite_data_path = input()
print('\n')
print('Input path to JAR file:')
jar_path = input()
print('\n')


# In[ ]:


id_num = 25
data_range = 100
row_num = 500


# In[ ]:


def generate_dataset(id_num, data_range, row_num):
    dataset = []
    for i in range(row_num):
        entry_id = int(np.ceil(np.random.uniform(0, id_num)))
        # Начало - Конец = 15.12.18 с 00,00 по 23,59
        timestamp = randint(1544832000, 1544918399) 
        data_volume = int(np.ceil(np.random.uniform(1, data_range))) * 10

        dataset.append([entry_id, timestamp, data_volume])
    
    return dataset


# In[ ]:


with open(raw_data_path, "w", newline="") as fout:
    writer = csv.writer(fout)
    writer.writerows(generate_dataset(id_num, data_range, row_num))
print('Dataset is generated!:\n')


# ### Data filtering

# In[ ]:


def filter_by_scale(scale, dataframe):
    min_timestamp = min(dataframe['timestamp'].tolist())
    return dataframe[dataframe['timestamp'] <= min_timestamp + scale], min_timestamp, scale


# In[ ]:


scaler = {'minute':60, 'hour':3600, 'half-day':43200, 'day':86400}


# In[ ]:


with open(raw_data_path) as fout:
    file = fout.readlines()
file = [row.strip().split(',') for row in file]
file = pd.DataFrame(file, columns=['id', 'timestamp', 'value']).astype('int64')


# In[ ]:


print('Please, input scale: \n')
print('Choose "minute", "hour", "half-day" or "day" \n')
scale_s = input()
assert scale_s in ['minute', 'hour', 'half-day', 'day'], 'Error, scale is not correct!'
filtered, min_timestamp, scale = filter_by_scale(scaler[scale_s], file)
print(str(filtered.shape[0]) + ' of ' + str(file.shape[0]) + ' rows')
raw_data_path = raw_data_path.split('\\')
raw_data_path[-1] = 'temp.txt'
raw_data_path = '\\'.join(raw_data_path)
filtered.to_csv(raw_data_path, sep=',', header=False, index=False)


# ### Ignite using 

# In[ ]:


subprocess.call(['java', '-jar', jar_path, raw_data_path, ignite_data_path])


# In[ ]:


print('Task complited!')


# ### Testing

# In[ ]:


def is_equal(python_aggregated_dataframe, data_from_ignite):
    comprehension = np.array(python_aggregated_dataframe['value'].tolist()) - np.array(data_from_ignite['value'].tolist())
    if np.sum(comprehension) != 0:
        print('Test failed!')
    else:
        print('Test complited!')


# In[ ]:


with open(raw_data_path) as fout:
    file = fout.readlines()
file = [row.strip().split(',') for row in file]
file = pd.DataFrame(file, columns=['id', 'timestamp', 'value']).astype('int64')


# In[ ]:


python_aggregated_dataframe = file.groupby('id')[['value']].sum()
python_aggregated_dataframe = python_aggregated_dataframe.sort_index()


# In[ ]:


with open(ignite_data_path) as fout:
    data_from_ignite = fout.readlines()
data_from_ignite = [row.strip().split(',') for row in data_from_ignite]
data_from_ignite = pd.DataFrame(data_from_ignite, columns=['id', 'value'], dtype=int)
data_from_ignite = data_from_ignite.sort_values('id')


# In[ ]:


is_equal(python_aggregated_dataframe, data_from_ignite)


# In[ ]:


data_from_ignite['timestamp'] = pd.Series(np.array([min_timestamp + scale]*data_from_ignite.shape[0]), index=data_from_ignite.index)
data_from_ignite['scale'] = pd.Series(np.array([scale_s]*data_from_ignite.shape[0]), index=data_from_ignite.index)
data_from_ignite = data_from_ignite[['id', 'timestamp', 'scale', 'value']]
data_from_ignite.to_csv(ignite_data_path, sep=',', header=False, index=False)
os.remove(raw_data_path)

